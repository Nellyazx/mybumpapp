package xyz.designabc.lottieonboarding.dashboard;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.board.api.ApiService;
import xyz.designabc.lottieonboarding.overflow.Contacts;
import xyz.designabc.lottieonboarding.utils.MySingleTone;

public class HomeFragment extends Fragment
{
    String gotEmergency;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View v = inflater.inflate(R.layout.fragment_home,container,false);
        FloatingActionButton callButton = v.findViewById(R.id.fab);

        callButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:0707 100 100"));
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    Toast.makeText(getActivity(), "Error calling", Toast.LENGTH_SHORT).show();

                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(callIntent);
            }
        });
        return v;

    }
//    public String getEmergencyContact()
//    {
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiService.GETFIRST_NAME, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                try
//                {
//                    JSONObject jsonObject = new JSONObject(response);
//                    JSONArray array = jsonObject.getJSONArray("results");
//                    JSONObject myObject = array.getJSONObject(0);
//                    gotEmergency = myObject.getString("emergency");
//                    // Toast.makeText(Dashboard.this, "my timeline "+timeLIneName,
//                    //Toast.LENGTH_LONG).show();
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                // progressDialog.dismiss();
//                Toast.makeText(getActivity(), "OOPs! Something went wrong. Connection Problem."+error.getMessage(), Toast.LENGTH_LONG).show();
//                error.printStackTrace();
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("email", getmail);
//                return params;
//            }
//        };
//        MySingleTone.getInstance(getActivity()).addToRequestQueue(stringRequest);
//        return gotEmergency;
//    }
}
