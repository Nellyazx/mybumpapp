package xyz.designabc.lottieonboarding.dashboard;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.board.api.ApiService;
import xyz.designabc.lottieonboarding.login_template_01.LoginActivity;
import xyz.designabc.lottieonboarding.overflow.AdditionalInfo;
import xyz.designabc.lottieonboarding.overflow.AdditionalInformation;
import xyz.designabc.lottieonboarding.overflow.ChildAdoptionServices;
import xyz.designabc.lottieonboarding.overflow.Contacts;
import xyz.designabc.lottieonboarding.overflow.Lamaze;
import xyz.designabc.lottieonboarding.overflow.LegalAdvice;
import xyz.designabc.lottieonboarding.overflow.MyProfile;
import xyz.designabc.lottieonboarding.overflow.SixMonths;
import xyz.designabc.lottieonboarding.utils.MySingleTone;

public class Dashboard extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener
{
    android.support.v7.widget.Toolbar toolbar;
    FloatingActionButton callButton;
    String gotFirst,gotLast,getEmergency;
    String timeLIneName,getmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        toolbar = findViewById(R.id.toolbar);
        Intent myData = getIntent();
        gotFirst = myData.getStringExtra("fname");
        gotLast = myData.getStringExtra("lname");
        Intent intent =getIntent();
        getmail = intent.getStringExtra("email");
        getFirstName();
        String myNameUp=getFirstName();
        setSupportActionBar(toolbar);        // toolbar.setTitle("Beryl's Timeline");
        //Toast.makeText(Dashboard.this, "my Name Up is "+myNameUp, Toast.LENGTH_SHORT).show();

        getSupportActionBar().setTitle(gotFirst+"'s TIMELINE");
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        loadFragment(new HomeFragment());
    }
    public String getFirstName()
    {
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ApiService.GETFIRST_NAME, new Response.Listener<String>() {
            @Override
            public void onResponse(String response)
            {
                try
                {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray array = jsonObject.getJSONArray("results");
                    JSONObject myObject = array.getJSONObject(0);
                    timeLIneName = myObject.getString("fname");
                   // Toast.makeText(Dashboard.this, "my timeline "+timeLIneName,
                            //Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                // progressDialog.dismiss();
                Toast.makeText(Dashboard.this, "OOPs! Something went wrong. Connection Problem."+error.getMessage(), Toast.LENGTH_LONG).show();
                error.printStackTrace();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("email", getmail);
                return params;
            }
        };
        MySingleTone.getInstance(getApplicationContext()).addToRequestQueue(stringRequest);
        return timeLIneName;
    }
    public boolean loadFragment(Fragment fragment)
    {
        if(fragment!=null)
        {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container,fragment)
                    .commit();
            return true;
        }
        return false;
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item)
    {
        Fragment fragment=null;
        switch(item.getItemId())
        {
            case R.id.navigation_home:
                String myName=getFirstName().toUpperCase();
                toolbar.setTitle(myName+"'s TIMELINE");
                fragment=new HomeFragment();
                loadFragment(fragment);
                return true;
            case R.id.navigation_calendar:
                toolbar.setTitle("Appointment");
                fragment = new CalendarFragment();
                loadFragment(fragment);
                return true;
            case R.id.navigation_maps:
                toolbar.setTitle("Google Maps");
                fragment = new MapFragment();
                loadFragment(fragment);
                return true;
            case R.id.navigation_chats:
                toolbar.setTitle("Chats");
                fragment = new ChatFragment();
                loadFragment(fragment);
                return true;
        }
        return false;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.overflow, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.myprofile:
                //  navigate to home stock list page
                //Intent intent = new Intent(StocksActivity.this,MoreInfo.class);
                //startActivity(intent);
                Intent myprofile = new Intent(Dashboard.this, MyProfile.class);
                startActivity(myprofile);
                return true;
            case R.id.allergy:
                //  navigate to home stock list page
                //Intent intent = new Intent(StocksActivity.this,MoreInfo.class);
                //startActivity(intent);
                Intent intent = new Intent(Dashboard.this, AdditionalInformation.class);
                startActivity(intent);
                return true;
            case R.id.contacts:
                //toolbar.setTitle("Market");
                //  navigate to home stock list page
                Intent intenttwo = new Intent(Dashboard.this,Contacts.class);
                startActivity(intenttwo);
                return true;
            case R.id.classes:
                //  toolbar.setTitle("Watch List");
                Intent lamaze = new Intent(Dashboard.this,Lamaze.class);
                startActivity(lamaze);
                return true;
            case R.id.childadoption:
                Intent childadoption = new Intent(Dashboard.this,ChildAdoptionServices.class);
                startActivity(childadoption);
                return true;

            case R.id.sixmon:
                Intent sixmon = new Intent(Dashboard.this,SixMonths.class);
                startActivity(sixmon);
                return true;
            case R.id.legal:
                Intent legal = new Intent(Dashboard.this,LegalAdvice.class);
                startActivity(legal);
                return true;
//            case R.id.action_logout:
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(StocksActivity.this);
//                builder.setTitle("Logout");
//                builder.setCancelable(false);
//                builder.setMessage("Are you sure you want to logout?");
//
//                builder.setPositiveButton("YES", new DialogInterface.OnClickListener()
//                {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        progressDialog = ProgressDialog.show(StocksActivity.this, "", "Logging out...", false, false);
//
//                        myDb.open();
//                        myDb.deleteUser();
//                        myDb.close();
//
//                        new Handler().postDelayed(new Runnable() {
//                            public void run() {
//                                progressDialog.dismiss();
//                                Intent i = new Intent(StocksActivity.this, Login.class);
//                                startActivity(i);
//                            }
//                        }, 2000);
//                    }
//                });
//                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.dismiss();
//                    }
//                });
//
//                AlertDialog dialog = builder.create();
//                dialog.show();
//
//                return true;

        }
        return super.onOptionsItemSelected(item);
    }


}
