package xyz.designabc.lottieonboarding.overflow;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toolbar;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.dashboard.Dashboard;

public class AdditionalInformation extends AppCompatActivity
{
    private FloatingActionButton linearLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_information);
        linearLayout = findViewById(R.id.signup_button);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);        // toolbar.setTitle("Beryl's Timeline");
        getSupportActionBar().setTitle("Additional Info");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Dashboard.class));
            }
        });
        linearLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(AdditionalInformation.this,NextPage.class);
                startActivity(intent);
            }
        });
    }
}
