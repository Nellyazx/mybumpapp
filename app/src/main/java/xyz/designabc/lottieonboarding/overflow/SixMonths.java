package xyz.designabc.lottieonboarding.overflow;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.dashboard.Dashboard;

public class SixMonths extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_six_months);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);        // toolbar.setTitle("Beryl's Timeline");
        getSupportActionBar().setTitle("Six Months Baby Care");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Dashboard.class));
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.overflow,menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.allergy:
                Intent intent = new Intent(SixMonths.this, AdditionalInformation.class);
                startActivity(intent);
                break;
            case R.id.contacts:
                Intent secintent = new Intent(SixMonths.this,Contacts.class);
                startActivity(secintent);
                break;
            case R.id.childadoption:
                Intent childadoption = new Intent(SixMonths.this,ChildAdoptionServices.class);
                startActivity(childadoption);
                break;
            case R.id.legal:
                Intent legal = new Intent(SixMonths.this,LegalAdvice.class);
                startActivity(legal);
                break;
            case R.id.classes:
                Intent lamaze = new Intent(SixMonths.this,Lamaze.class);
                startActivity(lamaze);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
