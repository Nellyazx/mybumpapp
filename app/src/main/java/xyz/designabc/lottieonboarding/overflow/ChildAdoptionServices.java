package xyz.designabc.lottieonboarding.overflow;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.dashboard.Dashboard;

public class ChildAdoptionServices extends AppCompatActivity
{
    Button procedure;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_child_adoption_services);
        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
        procedure = findViewById(R.id.procedure);
        setSupportActionBar(toolbar);        // toolbar.setTitle("Beryl's Timeline");
        getSupportActionBar().setTitle("Child Adoption Services");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), Dashboard.class));
            }
        });
        procedure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChildAdoptionServices.this,Procedure.class);
                startActivity(intent);
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.overflow,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch(item.getItemId())
        {
            case R.id.allergy:
                Intent intent = new Intent(ChildAdoptionServices.this, AdditionalInformation.class);
                startActivity(intent);
                break;
            case R.id.contacts:
                Intent secintent = new Intent(ChildAdoptionServices.this,Contacts.class);
                startActivity(secintent);
                break;
            case R.id.legal:
                Intent legal = new Intent(ChildAdoptionServices.this,LegalAdvice.class);
                startActivity(legal);
                break;
            case R.id.classes:
                Intent lamaze = new Intent(ChildAdoptionServices.this,Lamaze.class);
                startActivity(lamaze);
                break;
            case R.id.sixmon:
                Intent sixmonths = new Intent(ChildAdoptionServices.this,SixMonths.class);
                startActivity(sixmonths);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
