package xyz.designabc.lottieonboarding.login_template_01;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.board.api.ApiService;
import xyz.designabc.lottieonboarding.utils.MySingleTone;

public class ForgotPassword extends AppCompatActivity {

    EditText token, password, confpassword;
    Button resetpass;
    String picktoken, pickpassword, pickconfpass;
    AlertDialog.Builder alertDialog;
    StringRequest  stringRequest;
    ProgressDialog progressDialog;
    Boolean isInternetPresent = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        token = (EditText) findViewById(R.id.token);
        password = (EditText) findViewById(R.id.password);
        confpassword = (EditText) findViewById(R.id.confirm_password);
        alertDialog = new AlertDialog.Builder(ForgotPassword.this);
        resetpass = (Button) findViewById(R.id.resetpass);

        resetpass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //get text from editext
                picktoken = token.getText().toString();
                pickpassword = password.getText().toString();
                pickconfpass = confpassword.getText().toString();
                // pickconfpass = confpassword.getText().toString();
                //check that all fields are filled
                if (picktoken.equals("") || pickpassword.equals("")|| pickconfpass.equals("")) {
                    alertDialog.setTitle("Something went Wrong...");
                    alertDialog.setMessage("Please Fill in All the Fields...");
                    token.setText("");
                    password.setText("");
                    confpassword.setText("");
                } else {
                    if (!(pickpassword.equals(pickconfpass))) {
                        alertDialog.setTitle("Something went Wrong...");
                        alertDialog.setMessage("Passwords Mismatch...");
                        displayAlert("reset_failed");
                    } else {
                        getData();
                    }
                }
                getData();

            }
        }); }
    public void getData() {
       
            progressDialog = ProgressDialog.show(ForgotPassword.this, "", "Resetting Your Password...", false, false);
            stringRequest = new StringRequest(Request.Method.POST, ApiService.RESET_PASS_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    progressDialog.dismiss();

                    try {

                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        String code = jsonObject.getString("code");

                        if (code.equals("reset_success")) {
                            alertDialog.setTitle("Password Reset");
                            // alertDialog.setMessage(message);
                            displayAlert(jsonObject.getString("message"));
                        } else {

                            Toast.makeText(ForgotPassword.this, "Error Resetting Password", Toast.LENGTH_SHORT).show();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("tokene", picktoken);
                    params.put("newpassword", pickpassword);
                    params.put("newpassword1", pickconfpass);
                    return params;
                }
            };


            MySingleTone.getInstance(ForgotPassword.this).addToRequestQueue(stringRequest);


    }

    public void displayAlert(final String code) {
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });
        AlertDialog theDialog = alertDialog.create();
        theDialog.show();
    }
}
