package xyz.designabc.lottieonboarding.login_template_01.Fragments;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.login_template_01.Baby;
import xyz.designabc.lottieonboarding.login_template_01.LoginActivity;
import xyz.designabc.lottieonboarding.login_template_01.Tools.MyTimeUtils;
import xyz.designabc.lottieonboarding.login_template_01.customfonts.MyEditText;
import xyz.designabc.lottieonboarding.login_template_01.customfonts.MyTextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment {

    EditText fname,lname;
    TextView dob;
    Spinner height,weight;
    String finame,laname,daob,yoheight,yoweight;
    ArrayAdapter<CharSequence> myAdapter;
    ArrayAdapter<CharSequence> weightAdapter;
    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_up, container, false);
        fname = view.findViewById(R.id.firstname);
        lname = view.findViewById(R.id.lastname);
        dob = view.findViewById(R.id.birthday);
        height = view.findViewById(R.id.height);
        weight = view.findViewById(R.id.weight);
        weightAdapter = ArrayAdapter.createFromResource(getActivity(),R.array.weight,android.R.layout.simple_list_item_1);
        weightAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        weight.setAdapter(weightAdapter);
        weight.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                yoweight = weight.getItemAtPosition(position).toString();
                // yoweight = weight.getSelectedItem().toString();
                //        getData();
                // }}
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
        myAdapter = ArrayAdapter.createFromResource(getActivity(),R.array.height,android.R.layout.simple_list_item_1);
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        height.setAdapter(myAdapter);
        height.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id)
            {
                // yoheight = height.getSelectedItem().toString();
                yoheight= weight.getItemAtPosition(position).toString();

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent)
            {

            }
        });
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().findViewById(R.id.clickable_birthday_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                birthDayClick();
            }
        });
        getView().findViewById(R.id.signup_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //getText
                finame = fname.getText().toString();
                laname = lname.getText().toString();
                daob = dob.getText().toString();

                Intent intent = new Intent(getActivity(), Baby.class);
                intent.putExtra("fname",finame);
                intent.putExtra("lname",laname);
                intent.putExtra("dob",daob);
                intent.putExtra("height",yoheight);
                intent.putExtra("weight",yoweight);
               // Toast.makeText(getActivity(), " "+finame+" "+laname+" "+daob+" "+yoheight+" "+yoweight, Toast.LENGTH_SHORT).show();
                startActivity(intent);

            }
        });
    }

    public void birthDayClick()
    {
        DatePickerFragment date = new DatePickerFragment();
        // Set Up Current Date Into dialog
        Calendar calender = Calendar.getInstance();
        Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        // Set Call back to capture selected date
        date.setCallBack(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                if (datePicker.isShown()) {
                    //Toast.makeText(getActivity(), "the edd is "+edd, Toast.LENGTH_SHORT).show();
                    String predictdate = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(year);
                    //selectedDate =  pickDate.getText().toString();
                    //((TextView) getView().findViewById(R.id.birthday_edittext)).setText(MyTimeUtils.formatDate(temp.getTimeInMillis(), MyTimeUtils.BIRTHDAY_FORMAT));
                    ((TextView) getView().findViewById(R.id.birthday)).setText(predictdate);
                }
            }
        });
        date.show(this.getFragmentManager(), "Date Picker");
    }
}
