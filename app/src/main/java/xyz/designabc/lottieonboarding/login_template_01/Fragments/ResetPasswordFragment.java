package xyz.designabc.lottieonboarding.login_template_01.Fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.board.api.ApiService;
import xyz.designabc.lottieonboarding.login_template_01.ForgotPassword;
import xyz.designabc.lottieonboarding.login_template_01.LoginActivity;
import xyz.designabc.lottieonboarding.login_template_01.customfonts.MyEditText;
import xyz.designabc.lottieonboarding.utils.MySingleTone;


/**
 * A simple {@link Fragment} subclass.
 */
public class ResetPasswordFragment extends Fragment
{
    String pickemail;
    AlertDialog.Builder alertDialog;
    ProgressDialog progressDialog;
    StringRequest stringRequest;
    EditText email;
    public ResetPasswordFragment()
    {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_reset_password, container, false);
        email = view.findViewById(R.id.email_edittext);
        //password = (EditText) findViewById(R.id.password);
        //confpassword = (EditText) findViewById(R.id.confirm_password);
        alertDialog = new AlertDialog.Builder(getActivity());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getView().findViewById(R.id.reset_pass_button).setOnClickListener(
                new View.OnClickListener()
                {
            @Override
            public void onClick(View view)
            {
                pickemail = email.getText().toString();
                getData();
            }
        });
    }
    public void getData() {

            progressDialog = ProgressDialog.show(getActivity(), "", "Resetting Password. Please check mail..", false, false);
            stringRequest = new StringRequest(Request.Method.POST, ApiService.FORGOT_PASS_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    progressDialog.dismiss();


                    try {

                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        String code = jsonObject.getString("code");

                        if (code.equals("get_email_success")) {
                            alertDialog.setTitle("Email Found");
                            // alertDialog.setMessage(message);
                            displayAlert(jsonObject.getString("message"));


                        } else {


                            Toast.makeText(getActivity(), "Error Getting Mail", Toast.LENGTH_SHORT).show();
    }
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("email", pickemail);
                    return params;
                }
            };

            MySingleTone.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

    public void displayAlert(final String message) {
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                startActivity(new Intent(getActivity(), ForgotPassword.class));            }
        });
        AlertDialog alertDiag = alertDialog.create();
        alertDiag.show();
    }


}
