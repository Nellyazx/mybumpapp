package xyz.designabc.lottieonboarding.login_template_01;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.login_template_01.Fragments.DatePickerFragment;

public class Baby extends AppCompatActivity
{
    FloatingActionButton next;
    EditText babyname;
    TextView dueDate;
    String pickeddat,nickname;
    String passedfname,passedlname,passeddob,passedweight,passedheight;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_baby);
        next = findViewById(R.id.signup_button);
        dueDate = findViewById(R.id.duedate);
        babyname = findViewById(R.id.babyname);


        Intent intent = getIntent();
        passedfname = intent.getStringExtra("fname");
        passedlname = intent.getStringExtra("lname");
        passeddob = intent.getStringExtra("dob");
        passedweight = intent.getStringExtra("weight");
        passedheight = intent.getStringExtra("height");

        dueDate.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                pickDate();
            }
        });
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nickname = babyname.getText().toString();
                pickeddat = dueDate.getText().toString();
                Intent intent = new Intent(Baby.this, FinalStep.class);
                intent.putExtra("nickname",nickname);
                intent.putExtra("edd",pickeddat);
                intent.putExtra("fname",passedfname);
                intent.putExtra("lname",passedlname);
                intent.putExtra("dob",passeddob);
                intent.putExtra("weight",passedweight);
                intent.putExtra("height",passedheight);
               // Toast.makeText(Baby.this, "baby name "+nickname+" "+pickeddat+" "+passedfname+" "+passedlname+" "+passeddob+" "+passedweight+" "+passedheight, Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });
    }
    public void pickDate() {

        DatePickerFragment date = new DatePickerFragment();
        // Set Up Current Date Into dialog
        Calendar calender = Calendar.getInstance();

        final Bundle args = new Bundle();
        args.putInt("year", calender.get(Calendar.YEAR));
        args.putInt("month", calender.get(Calendar.MONTH));
        args.putInt("day", calender.get(Calendar.DAY_OF_MONTH));
        date.setArguments(args);
        // Set Call back to capture selected date
        date.setCallBack(new DatePickerDialog.OnDateSetListener()
        {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth)
            {
                if (datePicker.isShown())
                {
                    int eddyear=year;
                    int eddmonth = ((monthOfYear+1)-3);
                    int edddate = (dayOfMonth+7);


                    Toast.makeText(Baby.this, eddyear+"-" +eddmonth+"-"+edddate , Toast.LENGTH_SHORT).show();
                    String predictdate = String.valueOf(dayOfMonth) + "-" + String.valueOf(monthOfYear + 1) + "-" + String.valueOf(year);
                    //selectedDate =  pickDate.getText().toString();
                    //((TextView) getView().findViewById(R.id.birthday_edittext)).setText(MyTimeUtils.formatDate(temp.getTimeInMillis(), MyTimeUtils.BIRTHDAY_FORMAT));
                    dueDate.setText(predictdate);
                }
            }
        });
        date.show(this.getSupportFragmentManager(), "Date Picker");
    }
}
