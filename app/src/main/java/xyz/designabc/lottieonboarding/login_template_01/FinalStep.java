package xyz.designabc.lottieonboarding.login_template_01;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.board.api.ApiService;
import xyz.designabc.lottieonboarding.dashboard.Dashboard;
import xyz.designabc.lottieonboarding.utils.MySingleTone;

public class FinalStep extends AppCompatActivity {
    FloatingActionButton signUp;
    EditText email,pass,confpass,emergency;
    AlertDialog.Builder alertDialog;
    String getmail,getpass,getconfpass,passedfname,passedlname,passeddob,passedweight,passedheight,passednickname,passededd,emerg;
    ProgressDialog progressDialog;
    StringRequest stringRequest;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_step);
        signUp = findViewById(R.id.signup_button);
        email = findViewById(R.id.email);
        emergency = findViewById(R.id.emergency);
        pass = findViewById(R.id.password);
        confpass = findViewById(R.id.confpass);
        progressDialog = new ProgressDialog(this);

        Intent intent = getIntent();
        passedfname = intent.getStringExtra("fname");
        passedlname = intent.getStringExtra("lname");
        passeddob = intent.getStringExtra("dob");
        passedweight = intent.getStringExtra("weight");
        passedheight = intent.getStringExtra("height");
        passednickname = intent.getStringExtra("nickname");
        passededd = intent.getStringExtra("edd");
        alertDialog = new AlertDialog.Builder(FinalStep.this);

        signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getmail = email.getText().toString();
                getpass = pass.getText().toString();
                emerg = emergency.getText().toString();
                getconfpass = confpass.getText().toString();

                if (email.equals("")||pass.equals("")||confpass.equals("")) {
                    alertDialog.setTitle("Something went Wrong...");
                    alertDialog.setMessage("Please Fill in All the Fields...");
                    displayAlert("input_error");
                } else {
                    if (!(getpass.equals(getconfpass))) {
                        alertDialog.setTitle("Something went Wrong...");
                        alertDialog.setMessage("Passwords Mismatch...");
                        displayAlert("reg_failed");
                    } else {
                        getData();
                    }
                }

            }
        });
    }
    public void getData() {


            progressDialog = ProgressDialog.show(FinalStep.this, "", "Registering..", false, false);
            stringRequest = new StringRequest(Request.Method.POST, ApiService.REGISTER_URL, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {

                    progressDialog.dismiss();

                    try {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        String code = jsonObject.getString("code");
                        String message = jsonObject.getString("message");
                        alertDialog.setTitle("Server Response");
                        alertDialog.setMessage(message);
                        displayAlert(code);
                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        Toast.makeText(FinalStep.this, "Error registering user check "+e.getMessage(), Toast.LENGTH_SHORT).show();

                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(FinalStep.this, "Error registering user "+error.getMessage(), Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("fname", passedfname);
                    params.put("lname", passedlname);
                    params.put("dob", passeddob);
                    params.put("height", passedheight);
                    params.put("weight", passedweight);
                    params.put("nickname", passednickname);
                    params.put("edd", passededd);
                    params.put("emergency",emerg);
                    params.put("email", getmail);
                    params.put("pass", getconfpass);

                    return params;
                }
            };

            MySingleTone.getInstance(FinalStep.this).addToRequestQueue(stringRequest);

    }
    public void displayAlert(final String code) {
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                if (code.equals("input_error")) {
                    pass.setText("");
                    confpass.setText("");

                } else if (code.equals("reg_success")) {
                    Intent intent = new Intent(FinalStep.this, Dashboard.class);
                    intent.putExtra("nickname",passednickname);
                    intent.putExtra("edd",passededd);
                    intent.putExtra("fname",passedfname);
                    intent.putExtra("lname",passedlname);
                    intent.putExtra("dob",passeddob);
                    intent.putExtra("emergency",emerg);
                    intent.putExtra("weight",passedweight);
                    intent.putExtra("height",passedheight);
                    intent.putExtra("email",getmail);
                    startActivity(intent);

                } else if (code.equals("reg_failed")) {
                    email.setText("");
                    pass.setText("");
                    confpass.setText("");
                }
            }
        });
        AlertDialog theDialog = alertDialog.create();
        theDialog.show();
    }
}
