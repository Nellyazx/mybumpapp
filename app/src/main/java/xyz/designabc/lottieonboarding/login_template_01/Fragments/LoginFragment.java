package xyz.designabc.lottieonboarding.login_template_01.Fragments;


import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.board.api.ApiService;
import xyz.designabc.lottieonboarding.dashboard.Dashboard;
import xyz.designabc.lottieonboarding.login_template_01.Baby;
import xyz.designabc.lottieonboarding.login_template_01.LoginActivity;
import xyz.designabc.lottieonboarding.login_template_01.customfonts.MyEditText;
import xyz.designabc.lottieonboarding.utils.MySingleTone;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    StringRequest stringRequest;
    AlertDialog.Builder alertDialog;
    ProgressDialog progressDialog;
    String getemail,getpassword;
    EditText email,password;
    public LoginFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view =  inflater.inflate(R.layout.fragment_login, container, false);
        email = view.findViewById(R.id.email_edittext);
        alertDialog = new AlertDialog.Builder(getActivity());
        password = view.findViewById(R.id.password_edittext);
        return view;
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        getView().findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getemail = email.getText().toString();
                getpassword = password.getText().toString();
                if (email.equals("") || password.equals("")) {
                    alertDialog.setTitle("Error");
                    displayAlert("Please Fill in all the Fields");
                } else {
                    getData();
                }
            }
        });
    }
    public void displayAlert(final String message) {
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                email.setText("");
                password.setText("");
            }
        });
        AlertDialog alertDiag = alertDialog.create();
        alertDiag.show();
    }

    public void getData()
    {
            progressDialog = ProgressDialog.show(getActivity(), "", "Logging in...", false, false);
            stringRequest = new StringRequest(Request.Method.POST, ApiService.LOGIN_URL, new Response.Listener<String>()
            {
                @Override
                public void onResponse(String response)
                {
                    progressDialog.dismiss();

                    try
                    {
                        JSONArray jsonArray = new JSONArray(response);
                        JSONObject jsonObject = jsonArray.getJSONObject(0);
                        String code = jsonObject.getString("code");

                        if (code.equals("login_failed"))
                        {
                            alertDialog.setTitle("Failed");
                            // alertDialog.setMessage(message);
                            displayAlert(jsonObject.getString("message"));
                        } else
                            {

                            Intent intent = new Intent(getActivity(),Dashboard.class);
                            intent.putExtra("email", getemail);
                                Toast.makeText(getActivity(), "my email is"+getemail, Toast.LENGTH_SHORT).show();

                                startActivity(intent);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    progressDialog.dismiss();
//                Toast.makeText(Login.this, "OOPs! Something went wrong. Connection Problem.", Toast.LENGTH_LONG).show();
                    error.printStackTrace();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("email", getemail);
                    params.put("password", getpassword);
                    return params;
                }
            };
            MySingleTone.getInstance(getActivity()).addToRequestQueue(stringRequest);


    }
}
