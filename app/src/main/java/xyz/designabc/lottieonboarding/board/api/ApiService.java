package xyz.designabc.lottieonboarding.board.api;

public class ApiService
{
    public static final String BASE_URL = "http://mybump.silverstar.co.ke/app/";


    public static final String REGISTER_URL = BASE_URL + "register.php";
    public static final String LOGIN_URL = BASE_URL + "login.php";
    public static final String GETFIRST_NAME = BASE_URL + "getfirstname.php";

    public static final String FORGOT_PASS_URL = BASE_URL+"forgotpassmobile.php";
    public static final String RESET_PASS_URL = BASE_URL+"passwordreset.php";

}
