package xyz.designabc.lottieonboarding.board;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xyz.designabc.lottieonboarding.R;

public class Fragment4 extends Fragment {
    private View rootView;

    public static Fragment4 newInstance() {
        Bundle args = new Bundle();
        Fragment4 fragment = new Fragment4();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_four, container, false);
        return rootView;
    }
}