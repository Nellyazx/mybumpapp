package xyz.designabc.lottieonboarding.board;

import android.content.Intent;
import android.graphics.drawable.Animatable2;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.ogaclejapan.smarttablayout.SmartTabLayout;

import xyz.designabc.lottieonboarding.R;
import xyz.designabc.lottieonboarding.login_template_01.LoginActivity;

public class OnboardingActivity extends AppCompatActivity {
    private ViewPager viewPager;
    private SmartTabLayout smartTabs;
    private ImageView ivImage;
    private TextSwitcher tvSimpleText;
    private TextView tvGetStarted;
    private FrameLayout tabIndicator;
    private String[] titles = {"Are you Worried About the Baby you are carrying?", "Worry No More",
    "Everything Is Gonna Be Alright", "MyBump helps manage the pregnancy for you.."};
    private TextView textView;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_onboard);


        tabIndicator = (FrameLayout) findViewById(R.id.tab_indicator);
        //ivImage = (ImageView) findViewById(R.id.iv_image);
        //ivImage.setImageResource(R.drawable.yatra);
        tvSimpleText = (TextSwitcher) findViewById(R.id.tv_simple_text);
        tvSimpleText.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                LayoutInflater inflater = LayoutInflater.from(OnboardingActivity.this);
                textView = (TextView) inflater.inflate(R.layout.item_text_view, null);
                return textView;
            }
        });

        tvSimpleText.setText(titles[0]);

        tvSimpleText.setInAnimation(this, R.anim.fade_in);
        tvSimpleText.setOutAnimation(this, R.anim.fade_out);


        tvGetStarted = (TextView) findViewById(R.id.tv_no_trip);
        tvGetStarted.setVisibility(View.GONE);
        final AnimatedVectorDrawable avd2 = (AnimatedVectorDrawable) tvGetStarted.getBackground();

        tvGetStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvGetStarted.setText("Loading...");
                avd2.start();
            }
        });

        avd2.registerAnimationCallback(new Animatable2.AnimationCallback() {
            @Override
            public void onAnimationEnd(Drawable drawable) {

                tvGetStarted.setText("Done");
                Intent intent = new Intent(OnboardingActivity.this, LoginActivity
                        .class);
                startActivity(intent);
            }
        });

        setUpTabs();
    }

    private void setUpTabs() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);

        CirclePagerAdapter businessTypesPagerAdapter = new CirclePagerAdapter(getSupportFragmentManager());
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tvSimpleText.setText(titles[position]);
                if (position == 2) {
                   // ivImage.setImageResource(R.drawable.yatra);
                }
                if (position == 3) {
                    tabIndicator.setVisibility(View.GONE);
                    tvGetStarted.setVisibility(View.VISIBLE);
                   // ivImage.setImageResource(R.drawable.yatra_red);
                    ((TextView) tvSimpleText.getChildAt(0)).setTextColor(getResources().getColor(R.color.app_dark_200));
                    textView.setTextColor(getResources().getColor(R.color.app_dark_200));
                } else {
                    tabIndicator.setVisibility(View.VISIBLE);
                    tvGetStarted.setVisibility(View.GONE);
                    ((TextView) tvSimpleText.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    textView.setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setAdapter(businessTypesPagerAdapter);

        populateScreen();
        smartTabs.setViewPager(viewPager);

        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(6);
    }

    private void populateScreen() {
        ViewGroup tab = (ViewGroup) findViewById(R.id.tab_indicator);
        tab.addView(LayoutInflater.from(this).inflate(R.layout.tab_layout_offer_circle, tab, false));

        smartTabs = (SmartTabLayout) findViewById(R.id.smart_tab_layout);
        smartTabs.setCustomTabView(new SmartTabLayout.TabProvider() {

            @Override
            public View createTabView(ViewGroup container, int position, PagerAdapter adapter) {
                View itemView = getLayoutInflater().inflate(R.layout.tab_item_offer_circle, container, false);
                return itemView;
            }
        });
    }
}
