package xyz.designabc.lottieonboarding.board;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class CirclePagerAdapter extends FragmentStatePagerAdapter {
    private Context context;

    public CirclePagerAdapter(FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return 4;
    }

    // Returns Fragment for selected position.
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return Fragment4.newInstance();
            case 1:
                return Fragment3.newInstance();
            case 2:
                return Fragment2.newInstance();
            default:
                return Fragment1.newInstance();
        }
    }
}